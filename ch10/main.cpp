#include "../std_lib_facilities.h"
struct Point {
	int x,y;
};
int main(){
	int x,y,norig;
	vector<Point> original_points;
	for (int i = 0; i < 7; i++){
		cout << endl;
		cout << "x: ";
		cin >> x;
		cout << "y: ";
		cin >> y;
		cout << endl;
		Point p = {x,y};
		original_points.push_back(p);
		norig = i;
	}
	cout << "original points: " << endl;
	for (int i = 0; i < 7; i++){
		cout << endl << "x: " << original_points[i].x << endl;
		cout << "y: " << original_points[i].y << endl;
	}
	ofstream file("mydata.txt");
	for (int i = 0; i < 7; i++){
		file << "x: " << original_points[i].x << endl;
		file << "y: " << original_points[i].y << endl;
	}
	file.close();
	vector<Point> processed_points;
	ifstream ifile("mydata.txt");
	string xd;
	int s,a,nproc;
	for (int i = 0; i < 7; i++){
		ifile >> xd >> s;
		ifile >> xd >> a;
		Point r = {s,a};
		processed_points.push_back(r);
		nproc = i;
	}
	cout << "processed points: " << endl;

	for (int i = 0; i < 7; i++){
		cout << endl << "x: " << processed_points[i].x << endl;
		cout << "y: " << processed_points[i].y << endl;
	}

	int eq = 0;

	for (int c = 0; c < 7; c++){
		if (original_points[c].x == original_points[c].x &&
			original_points[c].y == original_points[c].y)
			{
				eq++;	
			}
	}
	if (norig == nproc){
		cout << "the number of elements are equal." << endl;
	}else {
		cout << "Something is wrong!" << endl;
	}

	if (eq == 7){
		cout << "original points and processed points are equal." << endl;
	}else {
		cout << "Something is wrong!" << endl;
	}
}
