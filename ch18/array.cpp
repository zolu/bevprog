#include "../std_lib_facilities.h"
int ga[10] = {1,2,4,8,16,32,64,128,256,512};
void f(int arr[], int n){
	int la[10];
	int *p = new int[n];
	for (int i = 0; i < n; i++){
		la[i] = arr[i];
		p[i] = arr[i];
		cout << "la:"  << i+1 << ". element: " << la[i] << endl;
		cout << "p:" << i+1 << ". element: " << p[i] << endl;
		cout << endl;
	}
	delete[] p;
}

int fact(int x){
	int z;
	z = x;
	for (int k = 1; k < x; k++){
		z = z*k;
	}
	return z;
}

int main(){
	f(ga, 10);

	int aa[10];
	for (int i = 0; i < 10; i++){
		aa[i] = fact(i+1);
		//cout << aa[i] << " " << endl;
	}
	f(aa, 10);
}
