#include "../std_lib_facilities.h"
vector <int> gv = {1,2,4,8,16,32,64,128,256,512};
void f(vector <int> vec){
	vector <int> lv;
	lv = vec;
	vector <int> lv2;
	lv2 = lv;
	for (int i = 0; i < 10; i++){
		cout << "lv: "  << i+1 << ". element: " << lv[i] << endl;
		cout << "lv2: " << i+1 << ". element: " << lv2[i] << endl;
		cout << endl;
	}
}

int fact(int x){
	int z;
	z = x;
	for (int k = 1; k < x; k++){
		z = z*k;
	}
	return z;
}

int main(){
    f(gv);

	vector <int> vv(10);
	for (int i = 0; i < 10; i++){
		vv[i] = fact(i+1);
		//cout << vv[i] << " " << endl;
	}
	f(vv);
}
