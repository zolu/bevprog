#include "../std_lib_facilities.h"

struct Date {
	int y;
	int m;
	int d;
};

void init_day(Date& dd, int y, int m, int d){
	if (dd.m <= 12 && dd.m > 0 || d <= 31 && d > 0){
		dd.y = y;
		dd.m = m;
		dd.d = d;
	}
	else {
		cout << "not a valid date.";
	}
}

void add_day(Date& dd, int n){
	dd.d += n;
	if (dd.d > 31) 
	{
		dd.m++;
		dd.d -= 31;
		if (dd.m > 12)
		{
			dd.y++;
			dd.m -= 12;
		}
	}
}

ostream& operator<<(ostream& os, const Date& dd)
{
	 return os << dd.d << ", " << dd.m << ", " << dd.y << endl;
}

int main(){
	Date today;
	init_day(today, 1978, 6, 25);
	Date tomorrow;
	tomorrow.y = today.y;
	tomorrow.m = today.m;
	tomorrow.d = today.d;
	add_day(tomorrow, 1);
	cout << "today: " << today << endl;
	cout << "tomorrow: " << tomorrow<< endl;
}
