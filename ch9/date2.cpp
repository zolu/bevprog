#include "../std_lib_facilities.h"

struct Date {
	int y;
	int m;
	int d;
	
	Date(int y, int m, int d);
	void add_day(int n);
};

Date::Date(int y, int m, int d){
	if (m <= 12 && m > 0 || d <= 31 && d > 0){
		Date::y = y;
		Date::m = m;
		Date::d = d;
	}
	else {
		cout << "not a valid date.";
	}
}
void Date::add_day(int n){
	Date::d += n;
	if (Date::d > 31) 
	{
		Date::m++;
		Date::d -= 31;
		if (Date::m > 12)
		{
			y++;
			Date::m -= 12;
		}
	}
}

ostream& operator<<(ostream& os, const Date& dd)
{
	 return os << dd.d << ", " << dd.m << ", " << dd.y << endl;
}

int main(){
	Date today = {1978, 6, 25};
	Date tomorrow = {1978, 6, 25};
	tomorrow.add_day(1);
	cout << "today: " << today << endl;
	cout << "tomorrow: " << tomorrow<< endl;
}
