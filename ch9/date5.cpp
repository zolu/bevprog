#include "../std_lib_facilities.h"

enum class Month{
	jan = 1, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec
};

Month returnMonth(int month);

class Date {
private:
	int y;
	Month m;
	int d;
public:	
	Date(int y, Month m, int d);
	void add_day(int n);
	void add_month(int n);
	void add_year(int n);
	Month month() const {return m;}
	int day() const {return d;}
	int year() const  {return y;}
};

Date::Date(int y, Month m, int d){
	if (static_cast<int>(m) >= 1 || static_cast<int>(m) <= 12|| d <= 31 && d > 0){
		Date::y = y;
		Date::m = m;
		Date::d = d;
	}
	else {
		cout << "not a valid date.";
	}
}
void Date::add_day(int n){
	Date::d += n;
	if (Date::d > 31) 
	{
		returnMonth((static_cast<int>(Date::m) == 12) ? 1 : (static_cast<int>(Date::m) + 1));
		Date::d -= 31;
		if (static_cast<int>(Date::m) > 12)
		{
			Date::y++;
		}
	}
}

void Date::add_month(int n){
	for (int x = 0; x < n; x++){
		returnMonth((static_cast<int>(Date::m) == 12) ? 1 : (static_cast<int>(Date::m) + 1));
		Date::d -= 31;
		if (static_cast<int>(Date::m) > 12)
		{
			Date::y++;
		}
	}
}

void Date::add_year(int n){
	Date::y++;
}
Month returnMonth(int month)
{
 switch (month)
 {
 case 1:
  return Month::jan;
  break;
 case 2:
  return Month::feb;
  break;
 case 3:
  return Month::mar;
  break;
 case 4:
  return Month::apr;
  break;
 case 5:
  return Month::may;
  break;
 case 6:
  return Month::jun;
  break;
 case 7:
  return Month::jul;
  break;
 case 8:
  return Month::aug;
  break;
 case 9:
  return Month::sep;
  break;
 case 10:
  return Month::oct;
  break;
 case 11:
  return Month::nov;
  break;
 case 12:
  return Month::dec;
  break;
 default:
  cout << "Bad month" << endl;
 }
}

ostream& operator<<(ostream& os, Date& dd)
{
	 return os << dd.day() << ", " << static_cast<int>(dd.month()) << ", " << dd.year() << endl;
}

int main(){
	Date today = {1978, Month::jun, 25};
	Date tomorrow = {1978, Month::jun, 25};
	tomorrow.add_day(1);
	cout << "today: " << today << endl;
	cout << "tomorrow: " << tomorrow<< endl;
}
