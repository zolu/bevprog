#include "../std_lib_facilities.h"
void print_array10(ostream& os, int* a){
	for (int i = 0; i < 10; i++){
		os << "value " << a[i] << endl;
	}
}

void print_vector10(ostream& os, vector<int>& a){
	for (int i = 0; i < 10; i++){
		os << "value " << a[i] << endl;
	}
}

void print_array(ostream& os, int* a, int n){
	for (int i = 0; i < n; i++){
		os << "value " << a[i] << endl;
	}
}

void print_vector(ostream& os, vector<int>& a, int n){
	for (int i = 0; i < n; i++){
		os << "value " << a[i] << endl;
	}
}

int main (){
	int* ints = new int[10];
	for (int i = 0; i < 10; i++){
		ints[i] = i;
	}
	print_array10(cout, ints);
	cout << endl;
	delete[] ints;
	int* intsH = new int[10] {100, 101, 102, 103, 104, 105, 106, 107, 108, 109};
	print_array10(cout, intsH);
	cout << endl;
	delete[] intsH;
	int* intsEl= new int[11] {100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110};
	print_array(cout, intsEl, 11);
	cout << endl;
	delete[] intsEl;
	int* intsTw = new int[20] {100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119};
	print_array(cout, intsTw, 20);
	delete[] intsTw;
	cout << endl;

	vector<int> intsHv {100, 101, 102, 103, 104, 105, 106, 107, 108, 109};
	vector<int> intsElv {100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110};
	vector<int> intsTwv {100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119};

	cout << endl;
	print_vector10(cout, intsHv);
	cout << endl;
	print_vector(cout, intsElv, 11);
	cout << endl;
	print_vector(cout, intsTwv, 20);
	cout << endl;

}
