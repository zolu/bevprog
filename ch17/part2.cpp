#include "../std_lib_facilities.h"
void printArr(ostream& os, int *a, int n){
	for (int i = 0; i < n; i++){
		os << "address: "  << &a[i]  << " value: " << a[i] << endl;
	}
};
int main(){
	int s = 7;
	int *p1 = &s;

	cout << &p1 << endl;
	cout << *p1 << endl;

	int sin[7] = {1,2,4,8,16,32,64};
	int *p2 = sin;

	cout << endl << &p1 << endl;
	cout << *p2 << endl;

	cout << "\n\n";

	printArr(cout, sin, 7);

	int *p3 = p2;
	p2 = p1;
	p2 = p3;

	cout << "p1" << endl << &p1 << endl;
	cout << *p1 << endl;
	cout << "p2" << endl << &p2 << endl;
	cout << *p2 << endl;

	int tints1[10] = {1,2,4,8,16,32,64,128,256,512};
	p1 = tints1;
	int tints2[10]; 
	p2 = tints2;

	cout << endl;
	for (int i = 0; i < 10; i++){
		p2[i] = p1[i];
		cout << p1[i] << " : " << p2[i] << endl;
	}
	
	vector<int> vints {1,2,4,8,16,32,64,128,256,512};
	vector<int> vints2(10);
	
	p1 = &vints[0];
	p2 = &vints2[0];

	cout << endl;

	for (int i = 0; i < 10; i++){
		p2[i] = p1[i];
		cout << p1[i] << " : " << p2[i] << endl;
	}
}
